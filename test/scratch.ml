let main =
  let size = 1024 * 1024 * 5 in

  let blocks, read_capability =
    Eris.encode_string ~convergence_secret:Eris.null_convergence_secret
      ~block_size:`Large
      (* (String.make 1024 'a' ^ String.make 1023 'b') *)
      (String.make size 'a')
    (* "Hello world!" *)
  in

  let cbor = Eris_cbor.to_cbor ~read_capabilities:[ read_capability ] blocks in

  (* Format.printf "CBOR: %a\n" Cborl.pp cbor; *)
  let cbor_encoded = Cborl.write cbor |> String.of_seq in
  let cbor_size = String.length cbor_encoded in
  Format.printf "Size of CBOR encoding: %d (%f)\n" cbor_size
    (float_of_int cbor_size /. float_of_int size);

  print_endline @@ Eris.Read_capability.to_urn read_capability;

  let block_get ref =
    Format.printf "block_ref: %s\n" (Base32.encode_exn ~pad:false ref);
    List.assoc ref blocks
  in

  let d = Eris.Decoder.init ~block_get read_capability in

  Format.printf "pos: %d\n" (Eris.Decoder.pos d);

  Format.printf "length: %d\n" @@ Eris.Decoder.length d

(*
let* d = Eris.Decoder.seek_to_end d in
 * let* d = Eris.Decoder.seek 0 d in
 * 
 * let* s, d = Eris.Decoder.read 2 d in
 * Format.printf "read 2: %s\n" s;
 * 
 * let* s, d = Eris.Decoder.read 2 d in
 * Format.printf "read 2: %s\n" s;
 *)
(* let* s, d = Eris.Decoder.read 10 d in *)
(* (\* Format.printf "read 2: %s\n" s; *\)
 * let* d = Eris.Decoder.seek 16383 d in
 * (\* let* s, d = Eris.Decoder.read 10 d in *\)
 * (\* Format.printf "read 10: %s\n" s; *\)
 * Format.printf "pos: %a\n" Fmt.(option int) (Option.map Eris.Decoder.pos d); *)
(* let* s = Eris.decode_string ~block_ref read_capability in
 * Format.printf "lenghth: %d\n" (String.length s); *)
(* return_unit *)

let () = main
