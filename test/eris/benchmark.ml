(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let block_size_bytes = function `Small -> 1024 | `Large -> 32768

let test_content ~block_size () =
  let block_size = block_size_bytes block_size in
  let rec loop i () =
    Seq.Cons (String.make block_size (Char.chr i), loop ((i + 1) mod 255))
  in
  loop 0

let block_size_bytes = function `Small -> 1024 | `Large -> 32768

type t = {
  crypto : Eris_crypto.t;
  name : string;
  size : int;
  block_size : Eris.block_size;
}

let run_encode_benchmark t =
  (* the test content is generated in blocks of size block_size. How
     many blocks do we need? *)
  let block_count = t.size / block_size_bytes t.block_size in

  (* init the encoder *)
  let encoder =
    Eris.Encoder.init ~convergence_secret:Eris.null_convergence_secret
      t.block_size
  in

  (* Start measuring time *)
  let t0 = Benchmark.make 0L in

  (* encode *)
  Seq.iter
    (fun content -> ignore @@ Eris.Encoder.feed encoder content)
    (test_content ~block_size:t.block_size () |> Seq.take block_count);

  let _blocks, _read_capability = Eris.Encoder.finalize encoder in

  let b = Benchmark.sub (Benchmark.make 0L) t0 in

  let throughput =
    (* size in MiB *)
    float_of_int (t.size / (1024 * 1024)) /. b.utime
  in

  throughput

let run () =
  let crypto = Eris_crypto.init () in

  let benchmarks =
    [
      {
        crypto;
        name = "Encode 100MiB (block size 1 KiB)";
        size = 100 * 1024 * 1024;
        block_size = `Small;
      };
      {
        crypto;
        name = "Encode 100MiB (block size 32 KiB)";
        size = 100 * 1024 * 1024;
        block_size = `Large;
      };
      {
        crypto;
        name = "Encode 1GiB (block size 32 KiB)";
        size = 1024 * 1024 * 1024;
        block_size = `Large;
      };
    ]
  in

  Format.printf "ERIS benchmarks\n";

  List.iter
    (fun t ->
      Format.printf "%s: " t.name;
      let throughput = run_encode_benchmark t in
      Format.printf "%f MiB/s\n" throughput)
    benchmarks

let () = run ()
