(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Bos
module D = Decoders_yojson.Basic.Decode

let test_vectors =
  OS.Dir.contents ~rel:true (Fpath.v "eris-test-vectors")
  |> Result.get_ok |> List.map Fpath.basename |> List.sort String.compare

module Positive = struct
  type t = {
    id : int;
    name : string;
    description : string;
    content : string;
    convergence_secret : string;
    block_size : Eris.block_size;
    blocks : (string * string) list;
    urn : string;
  }

  let test_vectors =
    test_vectors
    |> List.filter (String.starts_with ~prefix:"eris-test-vector-positive")
    |> List.map (Fpath.( / ) (Fpath.v "./eris-test-vectors"))

  let decoder =
    let open D in
    let* id = field "id" int in
    let* name = field "name" string in
    let* description = field "description" string in
    let* content = field "content" string >|= Base32.decode_exn in
    let* convergence_secret =
      field "convergence-secret" string >|= Base32.decode_exn
    in
    let* block_size =
      field "block-size" int >>= function
      | 1024 -> succeed `Small
      | 32768 -> succeed `Large
      | _ -> fail "invalid block size"
    in
    let* urn = field "urn" string in
    let* blocks =
      field "blocks" (key_value_pairs string)
      >|= List.map (fun (ref, block) ->
              (Base32.decode_exn ref, Base32.decode_exn block))
    in
    succeed
      {
        id;
        name;
        description;
        content;
        convergence_secret;
        block_size;
        blocks;
        urn;
      }

  let read fpath =
    OS.File.read fpath |> Result.get_ok |> D.decode_string decoder
    |> Result.get_ok

  let to_test t =
    Alcotest.test_case t.name `Quick (fun () ->
        let _blocks, read_capability =
          Eris.encode_string ~block_size:t.block_size
            ~convergence_secret:t.convergence_secret t.content
        in

        (* encode *)
        Alcotest.check Alcotest.string "read capabaility matches" t.urn
          (Eris.Read_capability.to_urn read_capability);

        (* decode *)
        let block_get ref = List.assoc ref t.blocks in
        let s = Eris.decode_string ~block_get read_capability in

        Alcotest.check Alcotest.string "decoded content matches" t.content s;

        ())
end

module Negative = struct
  type t = {
    id : int;
    name : string;
    description : string;
    blocks : (string * string) list;
    urn : string;
  }

  let test_vectors =
    test_vectors
    |> List.filter (String.starts_with ~prefix:"eris-test-vector-negative")
    |> List.map (Fpath.( / ) (Fpath.v "./eris-test-vectors"))

  let expected_exceptions = [ (13, Not_found) ]

  let decoder =
    let open D in
    let* id = field "id" int in
    let* name = field "name" string in
    let* description = field "description" string in
    let* urn = field "urn" string in
    let* blocks =
      field "blocks" (key_value_pairs string)
      >|= List.map (fun (ref, block) ->
              (Base32.decode_exn ref, Base32.decode_exn block))
    in
    succeed { id; name; description; blocks; urn }

  let read fpath =
    OS.File.read fpath |> Result.get_ok |> D.decode_string decoder
    |> Result.get_ok

  let to_test t =
    Alcotest.test_case t.name `Quick (fun () ->
        let read_capability = Eris.Read_capability.of_urn t.urn in

        let expected_exception = List.assoc_opt t.id expected_exceptions in

        (* decode *)
        let block_get ref = List.assoc ref t.blocks in

        let failed =
          try
            let _decoded_content =
              Eris.decode_string ~block_get read_capability
            in
            false
          with e -> (
            match expected_exception with
            | Some exn ->
                assert (exn = e);
                true
            | None -> true)
        in

        assert failed;
        ())
end

module Large = struct
  type t = {
    name : string;
    block_size : Eris.block_size;
    size : int;
    urn : string;
  }

  let tests =
    [
      {
        name = "100MiB (block size 1KiB)";
        block_size = `Small;
        (* 100 MiB *)
        size = 100 * 1024 * 1024;
        urn =
          "urn:eris:BIC6F5EKY2PMXS2VNOKPD3AJGKTQBD3EXSCSLZIENXAXBM7PCTH2TCMF5OKJWAN36N4DFO6JPFZBR3MS7ECOGDYDERIJJ4N5KAQSZS67YY";
      };
      {
        name = "1GiB (block size 32KiB)";
        block_size = `Large;
        (* 1 GiB *)
        size = 1024 * 1024 * 1024;
        urn =
          "urn:eris:B4BL4DKSEOPGMYS2CU2OFNYCH4BGQT774GXKGURLFO5FDXAQQPJGJ35AZR3PEK6CVCV74FVTAXHRSWLUUNYYA46ZPOPDOV2M5NVLBETWVI";
      }
      (* Takes a long time. Only run it explicitly. *)
      (* {
       *   name = "256GiB (block size 32KiB)";
       *   block_size = `Large;
       *   (\* 256 GiB  *\)
       *   size = 256 * 1024 * 1024 * 1024;
       *   urn =
       *     "urn:eris:B4B5DNZVGU4QDCN7TAYWQZE5IJ6ESAOESEVYB5PPWFWHE252OY4X5XXJMNL4JMMFMO5LNITC7OGCLU4IOSZ7G6SA5F2VTZG2GZ5UCYFD5E";
       * }; *);
    ]

  let block_size_bytes = function `Small -> 1024 | `Large -> 32768

  let pseudo_random_content ~crypto ~key ~block_size () =
    (* internal block size of ChaCha20 is 64 bytes *)
    let counter_increment = block_size_bytes block_size / 64 in
    let null_nonce = String.make 12 (Char.chr 0x00) in
    let null_block =
      String.make (block_size_bytes block_size) (Char.chr 0x00)
    in
    let rec loop ic () =
      Seq.Cons
        ( Eris_crypto.chacha20 crypto ~key ~nonce:null_nonce ~counter:ic
            null_block,
          loop (ic + counter_increment) )
    in
    loop 0

  let to_test t =
    Alcotest.test_case t.name `Slow (fun () ->
        (* initialize crypto *)
        let crypto = Eris_crypto.init () in

        (* the key is the Blake2b hash of the test name *)
        let key = Eris_crypto.blake2b crypto t.name in

        (* the pseudo random content is generated in blocks of size
           block_size. How many blocks do we need? *)
        let block_count = t.size / block_size_bytes t.block_size in

        (* init the encoder *)
        let encoder =
          Eris.Encoder.init ~convergence_secret:Eris.null_convergence_secret
            t.block_size
        in

        (* encode *)
        Seq.iter
          (fun content -> ignore @@ Eris.Encoder.feed encoder content)
          (pseudo_random_content ~crypto ~key ~block_size:t.block_size ()
          |> Seq.take block_count);

        let _blocks, read_capability = Eris.Encoder.finalize encoder in

        Alcotest.check Alcotest.string "read capabaility matches" t.urn
          (Eris.Read_capability.to_urn read_capability);

        ())
end

let () =
  Alcotest.run "ERIS test vectors"
    [
      ( "positive",
        Positive.test_vectors |> List.map Positive.read
        |> List.map Positive.to_test );
      ( "negative",
        Negative.test_vectors |> List.map Negative.read
        |> List.map Negative.to_test );
      ("large", Large.tests |> List.map Large.to_test);
    ]
