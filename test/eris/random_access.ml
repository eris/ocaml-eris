(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let test_sequence offset len =
  String.init len (fun i -> Hashtbl.hash (offset + i) mod 255 |> Char.chr)

(* Up to 255 MiB *)
(* let size =
 *   QCheck.Gen.int_bound (1024 * 1024 * 255) |> QCheck.make ~print:string_of_int *)

module Blocks = Map.Make (String)

let block_get blocks ref = Blocks.find ref blocks

let encode_test_sequence ~block_size len =
  let encoder =
    Eris.Encoder.init ~convergence_secret:Eris.null_convergence_secret
      block_size
  in

  let bs = match block_size with `Large -> 32768 | `Small -> 1024 in

  let rec loop offset blocks =
    if offset < len then
      let c = min bs (len - offset) in
      let new_blocks =
        Eris.Encoder.feed encoder (test_sequence offset c) |> List.to_seq
      in
      loop (offset + c) (Blocks.add_seq new_blocks blocks)
    else blocks
  in

  let blocks = loop 0 Blocks.empty in
  let final_blocks, read_capability = Eris.Encoder.finalize encoder in
  let blocks = Blocks.add_seq (List.to_seq final_blocks) blocks in

  (blocks, read_capability)

let size_small =
  QCheck.Gen.int_bound (1024 * 1024 * 10) |> QCheck.make ~print:string_of_int

let reads_small = QCheck.(list @@ pair size_small small_nat)

let test_small =
  QCheck.Test.make ~count:20 ~name:"test_random_access (block_size = 1KiB)"
    QCheck.(pair size_small reads_small)
    (fun (length, reads) ->
      let blocks, read_capability =
        encode_test_sequence ~block_size:`Small length
      in

      let decoder =
        Eris.Decoder.init ~block_get:(block_get blocks) read_capability
      in

      let decoder_length = Eris.Decoder.length decoder in
      assert (Int.equal decoder_length length);

      let () =
        List.iter
          (fun (position', n) ->
            let position = position' mod (length - n) in

            let read_content, _decoder =
              Eris.Decoder.(seek position decoder |> read n)
            in

            Alcotest.(
              check int "read content has expected size" n
                (String.length read_content));

            let expected_content = test_sequence position n in

            Alcotest.(
              check string "read content matches expected" expected_content
                read_content);

            ())
          reads
      in

      true)

(* Up to 100 MiB *)
let size_large =
  QCheck.Gen.int_bound (1024 * 1024 * 100) |> QCheck.make ~print:string_of_int

let reads_large = QCheck.(list @@ pair size_large small_nat)

let test_large =
  QCheck.Test.make ~count:10 ~name:"test_random_access (block_size = 32KiB)"
    QCheck.(pair size_large reads_large)
    (fun (length, reads) ->
      let blocks, read_capability =
        encode_test_sequence ~block_size:`Large length
      in

      let decoder =
        Eris.Decoder.init ~block_get:(block_get blocks) read_capability
      in

      let decoder_length = Eris.Decoder.length decoder in
      Alcotest.(check int "Eris.Decoder.length matches" length decoder_length);

      let () =
        List.iter
          (fun (position', n) ->
            let position = position' mod (length - n) in

            let read_content, _decoder =
              Eris.Decoder.(seek position decoder |> read n)
            in

            Alcotest.(
              check int "read content has expected size" n
                (String.length read_content));

            let expected_content = test_sequence position n in

            Alcotest.(
              check string "read content matches expected" expected_content
                read_content);

            ())
          reads
      in

      true)

let () =
  Alcotest.run "ERIS random-access"
    [
      ( "random-access",
        List.map QCheck_alcotest.to_alcotest [ test_small; test_large ] );
    ]
