let main =
  let encoder =
    Eris.(Encoder.init ~convergence_secret:null_convergence_secret `Large)
  in

  ignore @@ Eris.Encoder.feed encoder "Hello world!";

  let _, read_capability = Eris.Encoder.finalize encoder in

  print_endline @@ Eris.Read_capability.to_urn read_capability;

  let _, read_capability =
    Eris.encode_string ~convergence_secret:Eris.null_convergence_secret
      ~block_size:`Small "Howdie"
  in

  print_endline @@ Eris.Read_capability.to_urn read_capability;

  ()

let () =
  ignore main;
  print_endline "hi"
