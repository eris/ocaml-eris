; SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: CC0-1.0

(use-modules
 (guix packages)
 (guix download)
 (guix git-download)
 (guix build-system gnu)
 (guix build-system dune)
 (guix build-system ocaml)
 ((guix licenses) #:prefix license:)
 (gnu packages license)
 (gnu packages ocaml)
 (gnu packages multiprecision)
 (gnu packages node)
 (gnu packages zig))

(define-public ocaml-cborl
  (package
    (name "ocaml-cborl")
    (version "0.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://inqlab.net/git/ocaml-cborl.git")
                     (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "15bw3s82qpwxs9q42sx9rm0qjk1kdy3zm8m283rc907mls0rjx1i"))))
    (build-system dune-build-system)
    (arguments '())
    (propagated-inputs
     (list ocaml-zarith gmp ocaml-fmt))
    (native-inputs
     (list ocaml-alcotest ocaml-qcheck))
    (home-page "https://inqlab.net/git/ocaml-cborl")
    (synopsis "OCaml CBOR library")
    (description 
     "The Concise Binary Object Representation (CBOR), as specified by
RFC 8949, is a binary data serialization format.  CBOR is similar to
JSON but serializes to binary which is smaller and faster to generate
and parse.  This package provides an OCaml implementation of CBOR.")
    (license license:isc)))

(define-public ocaml-base32
  (package
    (name "ocaml-base32")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://inqlab.net/git/ocaml-base32.git")
                     (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ccalgcnx178dmnb3523gv47xf0hbfry45pg7dix64bn86niq4b1"))))
    (build-system dune-build-system)
    (native-inputs
     (list ocaml-alcotest ocaml-qcheck))
    (home-page "https://inqlab.net/git/ocaml-base32.git")
    (synopsis "Base32 encoding for OCaml")
    (description "Base32 is a binary-to-text encoding that represents
binary data in an ASCII string format by translating it into a
radix-32 representation.  It is specified in RFC 4648.")
    (license license:isc)))

(define-public ocaml-ptime
  (package
  (name "ocaml-ptime")
  (version "0.8.5")
  (source
    (origin
      (method url-fetch)
      (uri "https://erratique.ch/software/ptime/releases/ptime-0.8.5.tbz")
      (sha256
        (base32
          "1fxq57xy1ajzfdnvv5zfm7ap2nf49znw5f9gbi4kb9vds942ij27"))))
  (build-system ocaml-build-system)
  (arguments
   `(#:build-flags (list "build" "--with-js_of_ocaml" "true" "--tests" "true")
     #:phases
     (modify-phases %standard-phases
       (delete 'configure))))
  (propagated-inputs
   `(("ocaml-result" ,ocaml-result)
     ("js-of-ocaml" ,js-of-ocaml)))
  (native-inputs
    `(("ocaml-findlib" ,ocaml-findlib)
      ("ocamlbuild" ,ocamlbuild)
      ("ocaml-topkg" ,ocaml-topkg)
      ("opam" ,opam)))
  (home-page "https://erratique.ch/software/ptime")
  (synopsis "POSIX time for OCaml")
  (description
    "Ptime offers platform independent POSIX time support in pure OCaml. It
provides a type to represent a well-defined range of POSIX timestamps
with picosecond precision, conversion with date-time values,
conversion with [RFC 3339 timestamps][rfc3339] and pretty printing to a
human-readable, locale-independent representation.")
  (license license:isc)))

(define-public ocaml-crunch
  (package
   (name "ocaml-crunch")
   (version "3.2.0")
   (home-page "https://github.com/mirage/ocaml-crunch")
   (source (origin
            (method git-fetch)
            (uri (git-reference
		  (url home-page)
		  (commit (string-append "v" version))))
            (sha256
             (base32
              "0xsa9gciszz66q8firfzqxdg7h9v8l95xp86ngr5p4sljrj03cx2"))))
   (build-system dune-build-system)
   ;; require mirage-kv
   (arguments '(#:tests? #f))
   (propagated-inputs (list ocaml-cmdliner ocaml-ptime))
   (native-inputs (list ocaml-lwt))
   (synopsis "Convert a filesystem into a static OCaml module")
   (description
    "`ocaml-crunch` takes a directory of files and compiles them into a standalone
OCaml module which serves the contents directly from memory.  This can be
convenient for libraries that need a few embedded files (such as a web server)
and do not want to deal with all the trouble of file configuration.")
   (license license:isc)))

(define-public ocaml-decoders
  (package
   (name "ocaml-decoders")
   (version "0.7.0")
   (home-page "https://github.com/mattjbray/ocaml-decoders")
   (source (origin
            (method git-fetch)
            (uri (git-reference
		  (url home-page)
		  (commit (string-append "v" version))))
            (sha256
             (base32
              "1h5q66nlapbjyqpjn93zpiwdvb9b2kxxgqw2jxyp6a5y815k5hfj"))))
   (build-system dune-build-system)
   (arguments `(#:tests? #f
		#:package "decoders"))
   (synopsis "Elm-inspired decoders for Ocaml")
   (description
    "This package provides a combinator library for \"decoding\" JSON-like values into
your own Ocaml types, inspired by Elm's `Json.Decode` and `Json.Encode`.")
   (license license:isc)))

(define-public ocaml-decoders-yojson
  (package
   (inherit ocaml-decoders)
   (name "ocaml-decoders-yojson")
   (build-system dune-build-system)
   (arguments `(#:tests? #f
		#:package "decoders-yojson"))
   (propagated-inputs (list ocaml-decoders ocaml-yojson))))

(define-public ocaml-alcotest-lwt
  (package
    (inherit ocaml-alcotest)
    (name "ocaml-alcotest-lwt")
    (arguments
     `(#:package "alcotest-lwt"
       #:test-target "."
       ;; TODO fix tests
       #:tests? #f))
    (propagated-inputs
     `(("ocaml-alcotest" ,ocaml-alcotest)
       ("ocaml-lwt" ,ocaml-lwt)
       ("ocaml-logs" ,ocaml-logs)))
    (native-inputs
     `(("ocaml-re" ,ocaml-re)
       ("ocaml-cmdliner" ,ocaml-cmdliner)))))

(define-public monocypher
  (package
   (name "monocypher")
   (version "4.0.1")
   (source (origin
	     (method url-fetch)
	     (uri
	      (string-append "https://monocypher.org/download/monocypher-"
			     version
			     ".tar.gz"))
	     (sha256
	      (base32
	       "1sf0rql0lvia98ll7f2w3phffhicvzvinwnpmjjxvkwrlvdrcmrg"))))
   (build-system gnu-build-system)
   (outputs `("out" "doc"))
   (arguments
    `(#:phases
      (modify-phases %standard-phases
	(delete 'configure)
	(replace 'install
	  (lambda* (#:key outputs #:allow-other-keys)
            (let ((out (assoc-ref outputs "out")))
	      (invoke "make" "install-lib" (string-append "PREFIX=" out))
	      (invoke "make" "install-pc" (string-append "PREFIX=" out)))))
	(add-after 'install 'install-doc
	  (lambda* (#:key outputs #:allow-other-keys)
            (let ((out (assoc-ref outputs "doc")))
	      (invoke "make" "install-doc" (string-append "PREFIX=" out))))))))
   (synopsis "Easy-to-use crypto library")
   (description
    "Monocypher is an easy-tu-use crypto library. It offers
functionality for Authenticated Encryption (ChaCha20 and Poly1305),
Hashing (Blake2b), Password Hashing (Argon2i), Public Key Cryptography
(X25519), Public Key Signatures (EdDSA, Ed25519) and more.")
   (license (list license:cc0
		  ;; alternatively 2-clause BSD
		  license:bsd-2))
   (home-page "https://monocypher.org/")))

(define-public ocaml-monocypher
  (package
   (name "ocaml-monocypher")
   (version "0.2.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                     (url "https://inqlab.net/git/ocaml-monocypher.git")
                     (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0pjqi7y8ghrjhipr1yf6vsnlp795k798xnpka7jbm49kjyplxv2f"))))
   (build-system dune-build-system)
   (arguments '())
   (propagated-inputs (list ocaml-ctypes monocypher))
   (native-inputs
    (list ocaml-alcotest))
   (home-page "https://inqlab.net/ocaml-monocypher.git")
   (synopsis "OCaml bindings to the Monocypher cryptographic library")
   (description "Monocypher is a cryptographic library. It provides functions
for authenticated encryption, hashing, password hashing and key derivation, key
exchange, and public key signatures.  This library provides OCaml bindings to
Monocypher using Ctypes.")
   (license license:bsd-2)))

(define-public ocaml-eris
  (package
    (name "ocaml-eris")
    (version "0.0.0")
    (source #f)
    (build-system dune-build-system)
    (arguments '())
    (native-inputs
     (list zig
	   ocaml-crunch
	   ocaml-decoders-yojson
	   ocaml-bos
	   ocaml-alcotest
	   ocaml-alcotest-lwt
	   ocaml-qcheck
	   ocaml-benchmark
	   node-lts
	   reuse))
    (propagated-inputs
     (list ocaml-ctypes
	   ocaml-monocypher
	   ocaml-base32
	   ocaml-cborl
	   ocaml-lwt
	   js-of-ocaml))
    (home-page "https://codeberg.org/eris/ocaml-eris")
    (synopsis "OCaml implementation of ERIS")
    (description #f)
    (license license:agpl3+)))

ocaml-eris
