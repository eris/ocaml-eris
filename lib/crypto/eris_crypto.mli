(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type t

val init : unit -> t
val blake2b : t -> ?key:string -> string -> string

val chacha20 :
  t -> key:string -> nonce:string -> ?counter:int -> string -> string
