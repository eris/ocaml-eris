(*
 * SPDX-FileCopyrightText: 2022, 2023 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 *
 * This module contains code from Brr
 * (https://erratique.ch/software/brr). The original copyright notice
 * is as follows:
 *
 * Copyright (c) 2018 The brr programmers
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *)

external pure_js_expr : string -> 'a = "caml_pure_js_expr"

module Jv = struct
  type t
  type prop = string

  let global = pure_js_expr "globalThis"
  let undefined = pure_js_expr "undefined"

  external get : t -> prop -> t = "caml_js_get"
  external new' : t -> t array -> t = "caml_js_new"
  external call : t -> string -> t array -> 'a = "caml_js_meth_call"
  external of_int : int -> t = "%identity"

  (* external of_string : string -> t = "caml_jsstring_of_string" *)
  external of_jv_array : t array -> t = "caml_js_from_array"

  (* let console_log t : unit = call (get global "console") "log" [| t |] *)
end

module ArrayBuffer = struct
  type t = Jv.t
end

module Uint8Array = struct
  type t = Jv.t

  let constructor = Jv.get Jv.global "Uint8Array"
  let create n : t = Jv.new' constructor Jv.[| of_int n |]

  let of_buffer ?offset ?length buffer =
    Jv.new' constructor
      [|
        buffer;
        Option.map Jv.of_int offset |> Option.value ~default:Jv.undefined;
        Option.map Jv.of_int length |> Option.value ~default:Jv.undefined;
      |]

  let set_uint8 t offset value : unit =
    Jv.(call t "set" [| of_jv_array [| of_int value |]; of_int offset |])

  let set_string t ?(offset = 0) s : unit =
    for i = 0 to String.length s - 1 do
      set_uint8 t (offset + i) (Char.code @@ String.get s i)
    done

  let of_string s =
    let array = create (String.length s) in
    set_string array s;
    array

  external to_string : t -> string = "caml_string_of_array"
end

module WebAssembly : sig
  module Module : sig
    type t
  end

  module Memory : sig
    type t

    val buffer : t -> ArrayBuffer.t
    val grow : t -> int -> unit
  end

  module Instance : sig
    type t

    val exports : t -> Jv.t
    val memory : t -> Memory.t
  end

  val compile : Uint8Array.t -> Module.t
  val instantiate : Module.t -> Instance.t
end = struct
  let wa = Jv.get Jv.global "WebAssembly"

  module Module = struct
    type t = Jv.t
  end

  module Memory = struct
    type t = Jv.t

    let buffer t = Jv.get t "buffer"
    let grow t page_increase = Jv.call t "grow" [| Jv.of_int page_increase |]
  end

  module Instance = struct
    type t = Jv.t

    let exports instance = Jv.get instance "exports"
    let memory instance = Jv.get (exports instance) "memory"
  end

  let compile array : Module.t =
    let constructor = Jv.get wa "Module" in
    Jv.new' constructor [| array |]

  let instantiate m : Instance.t =
    let constructor = Jv.get wa "Instance" in
    Jv.new' constructor [| m |]
end

type t = WebAssembly.Instance.t

(* Pre-compiled WASM module *)
let pre_compiled_wasm_module = ref None

let init () =
  let wasm_module =
    match !pre_compiled_wasm_module with
    | None ->
        let wasm_array =
          Content.read "eris_crypto.wasm" |> Option.get |> Uint8Array.of_string
        in
        let wasm_module = WebAssembly.compile wasm_array in
        pre_compiled_wasm_module := Some wasm_module;
        wasm_module
    | Some wasm_module -> wasm_module
  in

  let wasm_instance = WebAssembly.instantiate wasm_module in

  (* Grow memory by one page size to ensure that it is strictly larger
     than (64KiB + 32B + 12B) - what we need to encode inputs of size
     32KiB. *)
  WebAssembly.(Memory.grow (Instance.memory wasm_instance) 1);

  wasm_instance

let blake2b (instance : t) ?key input =
  let memory_buffer = WebAssembly.(Instance.memory instance |> Memory.buffer) in

  (* 32 bytes for the key at offset 32 *)
  let mem_key = Uint8Array.of_buffer ~offset:32 ~length:32 memory_buffer in
  let key_size =
    match key with
    | Some key ->
        assert (String.length key == 32);
        Uint8Array.set_string mem_key key;
        32
    | _ -> 0
  in

  (* next 32 bytes for the hash output *)
  let mem_output = Uint8Array.of_buffer ~offset:64 ~length:32 memory_buffer in

  (* input memory at offset 128 *)
  let mem_input =
    Uint8Array.of_buffer ~offset:128 ~length:(String.length input) memory_buffer
  in
  let input_size = String.length input in
  Uint8Array.set_string mem_input input;

  ignore
  @@ Jv.call
       (WebAssembly.Instance.exports instance)
       "eris_crypto_blake2b"
       Jv.
         [|
           (* input *)
           of_int 128;
           of_int input_size;
           (* key *)
           of_int 32;
           of_int key_size;
           (* output *)
           of_int 64;
         |];

  Uint8Array.to_string mem_output

let chacha20 (instance : t) ~key ~nonce ?(counter = 0) input =
  let memory_buffer = WebAssembly.(Instance.memory instance |> Memory.buffer) in

  (* Don't use first 32 bytes. If we do key is at memory position 0 -
     a valid null pointer (!). Something in the Zig code checks for null
     pointers and fails. Shift everything to not use null pointers. *)

  (* 32 bytes for the key *)
  assert (String.length key == 32);
  let mem_key = Uint8Array.of_buffer ~offset:32 ~length:32 memory_buffer in
  Uint8Array.set_string mem_key key;

  (* 12 bytes for the nonce (at offset 64) *)
  assert (String.length nonce == 12);
  let mem_nonce = Uint8Array.of_buffer ~offset:64 ~length:12 memory_buffer in
  Uint8Array.set_string mem_nonce nonce;

  (* Input starting at offset 128 *)
  let input_size = String.length input in
  let mem_input =
    Uint8Array.of_buffer ~offset:128 ~length:input_size memory_buffer
  in
  Uint8Array.set_string mem_input input;

  (* output follows right after input *)
  let mem_output =
    Uint8Array.of_buffer ~offset:(128 + input_size) ~length:input_size
      memory_buffer
  in

  ignore
  @@ Jv.call
       (WebAssembly.Instance.exports instance)
       "eris_crypto_chacha20"
       Jv.
         [|
           (* input *)
           of_int 128;
           of_int input_size;
           (* key *)
           of_int 32;
           (* nonce *)
           of_int 64;
           (* counter *)
           of_int counter;
           (* output *)
           of_int (128 + input_size);
         |];

  Uint8Array.to_string mem_output
