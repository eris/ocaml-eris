/// SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
///
/// SPDX-License-Identifier: AGPL-3.0-or-later

const std = @import("std");
const Blake2b = std.crypto.hash.blake2.Blake2b(256);
const ChaCha20 = std.crypto.stream.chacha.ChaCha20IETF;

export fn eris_crypto_blake2b(input: [*]u8,
                              input_size: usize,
                              key: [*]u8,
                              key_size: usize,
                              output: [*]u8) void {

    var h : Blake2b = undefined;

    if (key_size == 0) {
        h = Blake2b.init(Blake2b.Options{});
    } else {
        h = Blake2b.init(Blake2b.Options{
            .key = key[0..key_size]
        });
    }

    Blake2b.update(&h, input[0..input_size]);

    var output_s = output[0..32];
    Blake2b.final(&h, output_s);
}


export fn eris_crypto_chacha20(input: [*]u8, input_size: usize,
                               key: [*]u8, nonce: [*]u8, counter: u32,
                               output: [*]u8) void {

    var input_s = input[0..input_size];
    var output_s = output[0..input_size];
    var key_s = key[0..32];
    var nonce_s = nonce[0..12];

    ChaCha20.xor(output_s, input_s, counter, key_s.*, nonce_s.*);
}
