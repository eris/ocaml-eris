// SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

#include <stddef.h>
#include <stdint.h>

void eris_crypto_blake2b(uint8_t *input, size_t input_size,
			 uint8_t *key, size_t key_size,
			 uint8_t *output);

void eris_crypto_chacha20(uint8_t *input, size_t input_size,
			  uint8_t *key, uint8_t *nonce, uint32_t counter,
			  uint8_t *output);
