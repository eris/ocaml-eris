(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Ctypes

type t = unit

let init () = ()

let blake2b () ?key input =
  let out_ptr = allocate_n char ~count:32 in

  let key_size = Option.(key |> map String.length |> value ~default:0) in

  let () =
    Zig.Function.eris_crypto_blake2b input
      (Unsigned.Size_t.of_int @@ String.length input)
      key
      (Unsigned.Size_t.of_int key_size)
      out_ptr
  in

  string_from_ptr out_ptr ~length:32

let chacha20 () ~key ~nonce ?(counter = 0) input =
  assert (String.length key == 32);
  assert (String.length nonce == 12);

  let input_size = String.length input in
  let out_ptr = allocate_n char ~count:input_size in

  let () =
    Zig.Function.eris_crypto_chacha20 input
      (Unsigned.Size_t.of_int input_size)
      key nonce
      (Unsigned.UInt32.of_int counter)
      out_ptr
  in

  string_from_ptr out_ptr ~length:input_size
