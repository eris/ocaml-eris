(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type t = unit

let init () = ()
let blake2b () ?key = Monocypher.Hashing.Blake2b.digest ?key ~size:32

let chacha20 () ~key ~nonce ?(counter = 0) =
  Monocypher.Advanced.IETF_ChaCha20.crypt ~key ~nonce ~ctr:counter
