(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module type IO = sig
  (** IO Monad *)

  type 'a t

  val return : 'a -> 'a t
  val fail : exn -> 'a t
  val map : ('a -> 'b) -> 'a t -> 'b t
  val bind : 'a t -> ('a -> 'b t) -> 'b t
end

module type S = sig
  (** This module provides a random-access decoder that can be used to efficiently decode pieces of content at given positions.

@see <http://purl.org/eris#name-random-access-decoding> Random-Access Decoding in the ERIS specification.
   *)

  type 'a io
  (** Type for IO *)

  type t
  (** Type of a decoder *)

  val init : block_get:(string -> string io) -> Read_capability.t -> t
  (** [init ~block_get read_capability] returns a new decoder.

Initializing a decoder will not cause any block de-references (the [block_get] function will not be called). *)

  val pos : t -> int
  (** [pos decoder] returns the position of the decoder as offset from the start of the encoded content. *)

  val seek : int -> t -> t io
  (** [seek pos decoder] returns a new decoder that is positioned at [pos].*)

  val length : t -> int io
  (** [length decoder] returns the length of the encoded content. *)

  val read : int -> t -> (string * t) io
  (** [read n decoder] reads at most [n] bytes of encoded content from position of [decoder]. Returns the decoded content as string as well as a new decoder with position set directly after end of content returned. *)
end

module Make (IO : IO) = struct
  (* Open IO Monad and add some syntax helpers *)

  type 'a io = 'a IO.t

  let ( let* ) = IO.bind
  let ( >>= ) = IO.bind
  let ( >|= ) p f = IO.map f p
  let fail_with msg = IO.fail (Failure msg)

  module Crypto = Eris_crypto

  type block = string
  type node = ReferenceKey of string * string | LeafNode of int * string

  type t = {
    (* Context *)
    crypto : Crypto.t;
    rc : Read_capability.t;
    block_get : string -> block IO.t;
    (* Location zipper *)
    left : node list;
    cur : node;
    up : t option;
    right : node list;
    (* Additional context of current location *)
    level : int;
    level_count : int;
    is_last : bool;
  }

  let nonce level =
    Seq.(append (return level) (take 11 @@ repeat 0))
    |> Seq.map Char.chr |> String.of_seq

  let null_ref_key = String.make 64 (Char.chr 0)

  let decode_internal_node t node =
    let arity = t.rc.block_size / 64 in

    let ref_key_pairs =
      Seq.init arity (fun i -> String.sub node (64 * i) 64)
      |> Seq.take_while (fun ref_key ->
             not @@ String.equal null_ref_key ref_key)
      |> Seq.map (fun ref_key ->
             ReferenceKey (String.sub ref_key 0 32, String.sub ref_key 32 32))
      |> List.of_seq
    in

    (* Ensure that rest of node is null *)
    if
      not
      @@ (Seq.init arity (fun i -> String.sub node (64 * i) 64)
         |> Seq.drop (List.length ref_key_pairs)
         |> Seq.for_all (String.equal null_ref_key))
    then failwith "Invalid node padding";

    ref_key_pairs

  let unpad block =
    let rec padding_boundary i =
      if String.get_uint8 block i = 0x80 then i
      else if String.get_uint8 block i = 0x00 then padding_boundary (i - 1)
      else failwith "Invalid padding"
    in

    String.sub block 0 (padding_boundary @@ (String.length block - 1))

  let move_down t =
    let arity = t.rc.block_size / 64 in

    match t.cur with
    | LeafNode _ -> IO.fail @@ Failure "can not move down from LeafNode"
    | ReferenceKey (ref, key) -> (
        let* block = t.block_get ref in

        (* Check that dereferenced block is valid *)
        let check_ref = Crypto.blake2b t.crypto block in
        (* TODO: explicitly indicate a ref error *)
        assert (String.equal check_ref ref);
        assert (String.length block == t.rc.block_size);

        let node =
          Crypto.chacha20 t.crypto ~key ~nonce:(nonce (t.level - 1)) block
        in

        (* Format.printf "down - level:%d, level_count: %d, is_last: %b\n"
         *   (t.level - 1) t.level_count t.is_last; *)
        if t.level == 1 then
          (* dereferenced node is a leaf node *)
          let unpadded = if t.is_last then unpad node else node in
          IO.return
            {
              t with
              level = 0;
              level_count = t.level_count;
              (* leaf nodes do not have any neighbours to the left *)
              left = [];
              cur = LeafNode (0, unpadded);
              up = Some t;
              right = [];
              is_last = t.is_last;
            }
        else
          let nodes = decode_internal_node t node in
          (* Format.printf "nodes: %d\n" (List.length nodes); *)
          match nodes with
          | cur :: right ->
              IO.return
                {
                  t with
                  level = t.level - 1;
                  level_count = t.level_count * arity;
                  left = [];
                  cur;
                  up = Some t;
                  right;
                  is_last = t.is_last && List.length right == 0;
                }
          | [] -> fail_with "empty node encountered")

  let move_right t =
    match t.right with
    | cur :: right ->
        {
          t with
          left = t.cur :: t.left;
          cur;
          right;
          level_count = t.level_count + 1;
          is_last =
            Option.(
              map (fun up -> up.is_last && up.right = []) t.up
              |> value ~default:false)
            && List.length right == 0;
        }
    | [] -> failwith "can not move right"

  let move_left t =
    match t.left with
    | cur :: left ->
        {
          t with
          left;
          cur;
          right = t.cur :: t.right;
          level_count = t.level_count - 1;
          is_last = false;
        }
    | [] -> failwith "can not move left"

  let move_up t =
    match t.up with Some up -> up | None -> failwith "can not go up"

  let init ~block_get rc =
    let crypto = Crypto.init () in
    {
      crypto;
      rc;
      block_get;
      level = rc.level + 1;
      level_count = 0;
      left = [];
      cur = ReferenceKey (rc.root_reference, rc.root_key);
      up = None;
      right = [];
      is_last = true;
    }

  let rec pow base exponent =
    if exponent = 0 then 1 else base * pow base (exponent - 1)

  module Range = struct
    let within pos (start, end') = start <= pos && pos <= end'
    let above pos (_, end') = end' < pos
    let below pos (start, _) = pos < start
  end

  let level_rk_width t level =
    (* Size of content that one rk at this level can address *)
    let arity = t.rc.block_size / 64 in
    pow arity (level - 1) * t.rc.block_size

  let node_range t =
    (* Range that can be reached from current location and neighbors in same node *)
    match t.cur with
    | ReferenceKey _ ->
        let left_count = List.length t.left in
        let right_count = List.length t.right in

        let level_rk_width = level_rk_width t t.level in
        ( level_rk_width * (t.level_count - left_count),
          (level_rk_width * (t.level_count + right_count + 1)) - 1 )
    | LeafNode (_offset, content) ->
        let level_offset = t.rc.block_size * t.level_count in
        (level_offset, level_offset + String.length content - 1)

  let cur_range t =
    (* Range that can be reached from current location *)
    match t.cur with
    | ReferenceKey _ ->
        let level_rk_width = level_rk_width t t.level in
        ( level_rk_width * t.level_count,
          (level_rk_width * (t.level_count + 1)) - 1 )
    | LeafNode (_offset, content) ->
        ( t.rc.block_size * t.level_count,
          (t.rc.block_size * t.level_count) + String.length content - 1 )

  let pos t =
    match t.cur with
    | LeafNode (offset, _) -> (t.level_count * t.rc.block_size) + offset
    | ReferenceKey _ ->
        assert (t.level > 0);
        level_rk_width t t.level * t.level_count

  let seek_to_end t =
    let rec up_until_parent_is_last t =
      if t.is_last then t
      else
        (* Format.printf
         *   "up_until_parent_is_last - level: %d, level_count: %d, is_last: %b\n"
         *   t.level t.level_count t.is_last; *)
        let up = Option.get t.up in
        if up.is_last then t else up_until_parent_is_last @@ up
    in

    let rec all_the_way_right t =
      match t.right with [] -> t | _ -> all_the_way_right @@ move_right t
    in

    let rec all_the_way_down_right t =
      match t.cur with
      | LeafNode (_, content) ->
          IO.return
          @@ { t with cur = LeafNode (String.length content, content) }
      | ReferenceKey _ ->
          all_the_way_right t |> move_down >>= all_the_way_down_right
    in

    up_until_parent_is_last t |> all_the_way_down_right

  let rec seek to' t =
    let pos = pos t in

    (* Format.printf
     *   "seek %d - pos: %d, level: %d, level_count: %d, is_last: %b, cur_range: \
     *    (%a), node_range: (%a), full_node_range: (%a)\n"
     *   to' pos t.level t.level_count t.is_last
     *   Fmt.(pair ~sep:comma int int)
     *   (cur_range t)
     *   Fmt.(pair ~sep:comma int int)
     *   (node_range t)
     *   Fmt.(pair ~sep:comma int int)
     *   (full_node_range t); *)
    match t.cur with
    | LeafNode (offset, block) ->
        if Range.within to' (node_range t) then
          IO.return { t with cur = LeafNode (offset + (to' - pos), block) }
        else if Range.above to' (node_range t) && not t.is_last then
          (* go up and continue seeking *)
          move_up t |> seek to'
        else (* attempting to seek beyond content *)
          seek_to_end t
    | ReferenceKey _ ->
        if Range.within to' (cur_range t) then move_down t >>= seek to'
        else if Range.within to' (node_range t) && Range.below to' (cur_range t)
        then move_left t |> seek to'
        else if
          Range.within to' (node_range t)
          && Range.above to' (cur_range t)
          && not t.is_last
        then move_right t |> seek to'
        else if
          Range.within to' (node_range t)
          && Range.above to' (cur_range t)
          && t.is_last
        then (* Attempting to seek beyond the range of last node *)
          seek_to_end t
        else move_up t |> seek to'

  let length t = seek_to_end t >|= pos

  let read n t =
    (* Format.printf "read %d - level: %d\n" n t.level; *)

    (* seek to current position - ensures that we are at a leaf node *)
    let* t = seek (pos t) t in

    let rec do_read buffer n t =
      let pos = pos t in
      match t.cur with
      | LeafNode (offset, block) when t.is_last ->
          let remaining = String.length block - offset in
          if n <= remaining then (
            Buffer.add_string buffer (String.sub block offset n);
            seek (pos + n) t)
          else (
            Buffer.add_string buffer (String.sub block offset remaining);
            seek_to_end t)
      | LeafNode (offset, block) ->
          let remaining = String.length block - offset in
          if n <= remaining then (
            Buffer.add_string buffer (String.sub block offset n);
            seek (pos + n) t)
          else (
            Buffer.add_string buffer (String.sub block offset remaining);
            seek (pos + remaining) t >>= do_read buffer (n - remaining))
      | _ -> fail_with "expecting to be at a leaf node"
    in

    let buffer = Buffer.create n in
    let* t = do_read buffer n t in
    IO.return (Buffer.contents buffer, t)
end
