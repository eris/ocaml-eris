(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let spec_version = "1.0.0"

type block_size = [ `Large | `Small ]

module Read_capability = Read_capability

type ref = string
type block = string

let null_convergence_secret = String.make 32 (Char.chr 0)

module Encoder = Encoder

let rec string_chunk size string =
  if String.length string == 0 then Seq.empty
  else if String.length string < size then Seq.return string
  else
    Seq.cons (String.sub string 0 size)
      (string_chunk size @@ String.sub string size (String.length string - size))

let encode_string ~convergence_secret ~block_size input =
  let e = Encoder.init ~convergence_secret block_size in
  let block_size = match block_size with `Large -> 32768 | `Small -> 1024 in
  let encode_blocks =
    Seq.fold_left
      (fun blocks chunk -> blocks @ Encoder.feed e chunk)
      []
      (string_chunk block_size input)
  in
  let finalize_blocks, rc = Encoder.finalize e in
  (encode_blocks @ finalize_blocks, rc)

module Decoder = IODecoder.Make (struct
  type 'a t = 'a

  let return v = v
  let fail exn = raise exn
  let map f v = f v
  let bind m f = f m
end)

let decode_string ~block_get (read_capability : Read_capability.t) =
  let block_size = read_capability.block_size in

  (* allocate a buffer ten times the size of block_size - a ungrounded heuristic *)
  let buffer = Buffer.create (block_size * 10) in

  let rec do_read decoder =
    let s, decoder = Decoder.read block_size decoder in
    Buffer.add_string buffer s;
    (* stop reading if read string is smaller than block size *)
    if String.length s < block_size then () else do_read decoder
  in

  let decoder = Decoder.init ~block_get read_capability in
  let () = do_read decoder in

  Buffer.contents buffer

module IODecoder = IODecoder
