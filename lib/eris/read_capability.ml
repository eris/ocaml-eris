(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type t = {
  block_size : int;
  level : int;
  root_reference : string;
  root_key : string;
}

let make block_size level root_reference root_key =
  { block_size; level; root_reference; root_key }

let block_size t = t.block_size

let to_binary t =
  let b = Buffer.create 66 in
  let block_size_code =
    match t.block_size with
    | 1024 -> 0x0a
    | 32768 -> 0x0f
    | _ -> failwith "invalid block size"
  in
  Buffer.add_uint8 b block_size_code;
  Buffer.add_uint8 b t.level;
  Buffer.add_string b t.root_reference;
  Buffer.add_string b t.root_key;
  Buffer.contents b

let of_binary b =
  let block_size_code = String.get_uint8 b 0 in
  let block_size =
    match block_size_code with
    | 0x0a -> 1024
    | 0x0f -> 32768
    | _ -> failwith "invalid block size code"
  in
  let level = String.get_uint8 b 1 in
  let root_ref = String.sub b 2 32 in
  let root_key = String.sub b 34 32 in
  make block_size level root_ref root_key

let to_urn t = "urn:eris:" ^ (to_binary t |> Base32.encode_exn ~pad:false)

let of_urn urn =
  if String.starts_with ~prefix:"urn:eris:" urn then
    Base32.decode_exn ~off:9 ~len:106 urn |> of_binary
  else failwith "invalid ERIS URN"
