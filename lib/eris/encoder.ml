(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type block_size = [ `Large | `Small ]

type t = {
  convergence_secret : string;
  block_size : int;
  mutable levels : Buffer.t array;
  input_buffer : Buffer.t;
  crypto : Eris_crypto.t;
}

let init ~convergence_secret block_size =
  let crypto = Eris_crypto.init () in
  let block_size = match block_size with `Large -> 32768 | `Small -> 1024 in
  {
    convergence_secret;
    block_size;
    levels = [||];
    input_buffer = Buffer.create block_size;
    crypto;
  }

let nonce level =
  Seq.(append (return level) (take 11 @@ repeat 0))
  |> Seq.map Char.chr |> String.of_seq

let encrypt_leaf_node encoder leaf_node =
  let key =
    Eris_crypto.blake2b encoder.crypto ~key:encoder.convergence_secret leaf_node
  in
  let block =
    Eris_crypto.chacha20 encoder.crypto ~key ~nonce:(nonce 0) leaf_node
  in
  let ref = Eris_crypto.blake2b encoder.crypto block in
  (block, ref, key)

let encrypt_internal_node encoder level node =
  let key = Eris_crypto.blake2b encoder.crypto node in
  let block =
    Eris_crypto.chacha20 encoder.crypto ~key ~nonce:(nonce level) node
  in
  let ref = Eris_crypto.blake2b encoder.crypto block in
  (block, ref, key)

let add_ref_key_to_level encoder level ref key =
  if Array.length encoder.levels <= level then
    encoder.levels <-
      Array.append encoder.levels [| Buffer.create @@ encoder.block_size |]
  else ();

  let level_buffer = Array.get encoder.levels level in
  Buffer.add_string level_buffer ref;
  Buffer.add_string level_buffer key

let force_collect encoder level =
  let level_buffer = Array.get encoder.levels level in

  (* pad the node *)
  let unpadded_node_size = Buffer.length level_buffer in
  Buffer.add_seq level_buffer
    Seq.(take (encoder.block_size - unpadded_node_size) @@ repeat @@ Char.chr 0);

  (* get node *)
  let node = Buffer.sub level_buffer 0 encoder.block_size in

  (* clear level *)
  Buffer.clear level_buffer;

  (* encrypt node *)
  let block, ref, key = encrypt_internal_node encoder (level + 1) node in
  (block, ref, key)

let rec collect encoder level =
  if Array.length encoder.levels < level then
    (* level is not allocated yet, nothing to do *)
    []
  else
    let level_buffer = Array.get encoder.levels level in

    if Buffer.length level_buffer == encoder.block_size then (
      (* collect reference-key pairs to a node and encrypt to block *)
      let block, ref, key = force_collect encoder level in

      (* add reference to next level *)
      add_ref_key_to_level encoder (level + 1) ref key;

      (* continue collecting at next level *)
      [ (ref, block) ] @ collect encoder (level + 1))
    else []

let rec encode_buffer ?(blocks = []) encoder =
  let buffer_size = Buffer.length encoder.input_buffer in

  if buffer_size >= encoder.block_size then (
    let leaf_node = Buffer.sub encoder.input_buffer 0 encoder.block_size in

    (* split of leaf_node from buffer*)
    (* TODO this is not efficient, improve *)
    let rest =
      Buffer.sub encoder.input_buffer encoder.block_size
        (buffer_size - encoder.block_size)
    in
    Buffer.clear encoder.input_buffer;
    Buffer.add_string encoder.input_buffer rest;

    let block, ref, key = encrypt_leaf_node encoder leaf_node in

    add_ref_key_to_level encoder 0 ref key;

    let collected_blocks = collect encoder 0 in

    encode_buffer ~blocks:(((ref, block) :: collected_blocks) @ blocks) encoder)
  else blocks

let feed encoder input =
  (* add input to buffer *)
  Buffer.add_string encoder.input_buffer input;

  (* attempt to encode what is in buffer *)
  encode_buffer encoder

let pad_buffer encoder =
  let buffer_size = Buffer.length encoder.input_buffer in

  let required_padding = encoder.block_size - buffer_size - 1 in

  Buffer.add_char encoder.input_buffer (Char.chr 0x80);
  Buffer.add_string encoder.input_buffer
    (String.make required_padding (Char.chr 0x00))

let finalize encoder =
  (* pad buffer and encode *)
  pad_buffer encoder;

  let blocks = encode_buffer encoder in

  (* input buffer is empty now *)
  assert (Buffer.length encoder.input_buffer == 0);

  let rec do_finalize ?(blocks = []) level =
    let level_count = Array.length encoder.levels in
    let level_buffer = Array.get encoder.levels level in

    (* at top level and there is exactly one reference key pair *)
    if level_count = level + 1 && Buffer.length level_buffer == 64 then
      let ref = Buffer.sub level_buffer 0 32 in
      let key = Buffer.sub level_buffer 32 32 in

      let read_capability =
        Read_capability.make encoder.block_size level ref key
      in

      (blocks, read_capability)
    else if Buffer.length level_buffer > 0 then (
      (* collect reference-key pairs to a node and encrypt to block *)
      let block, ref, key = force_collect encoder level in

      add_ref_key_to_level encoder (level + 1) ref key;

      do_finalize ~blocks:((ref, block) :: blocks) (level + 1))
    else do_finalize ~blocks (level + 1)
  in

  let finalize_blocks, read_capability = do_finalize 0 in

  (blocks @ finalize_blocks, read_capability)
