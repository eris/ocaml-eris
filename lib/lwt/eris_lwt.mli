(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** Encoding for Robust Immutable Storage (ERIS)

ERIS is an encoding of arbitrary content into a set of uniformly
sized, encrypted and content-addressed blocks as well as a short
identifier that can be encoded as an URN. The content can be
reassembled from the blocks only with this identifier. The encoding is
defined independent of any storage and transport layer or any specific
application

@see <http://purl.org/eris> Specification of ERIS

 *)

(** {1 Basics} *)

val spec_version : string
(** Version of the ERIS specification implemented. *)

(** {2 Block Size} *)

(** ERIS uses two block sizes which must be specified when encoding content. *)

type block_size =
  [ `Large
    (** Block size of 32 KiB (32768 bytes). Use this if the content to be encoded is larger than 16 KiB. *)
  | `Small
    (** Block size of 1 KiB (1024 bytes). Use this if the content to be encoded is smaller than 16 KiB. *)
  ]

(** {2 Read Capability} *)

module Read_capability : sig
  (** A read capability contains the necessary information to decode some encoded content from blocks. A read capability can be encoded as a binary string or as an URN. *)

  type t
  (** Type of a read capability *)

  val block_size : t -> int
  (** [block_size read_capability] returns the block size in bytes. *)

  (** {1 Binary Encoding} *)

  val to_binary : t -> string
  val of_binary : string -> t

  (** {1 URN Encoding} *)

  val to_urn : t -> string
  val of_urn : string -> t
end

(** {2 Blocks and References} *)

type ref = string
(** Type of a reference to a block. A reference to a block is the
Blake2b-256 hash of the block. *)

type block = string
(** Type of a block. Length of a block is either 1024 bytes (for block
size [`Small]) or 32678 bytes (for block size [`Large]). *)

(** {1 Encoding} *)

(** Inputs to the encoding process are:

{ul {- Some content as string}
    {- A convergence-secret (a string of length 32 bytes)}
    {- The block size to use}}

The output is a read capability and a set of blocks. *)

val null_convergence_secret : string
(** The null convergence-secret (32 bytes of zeroes). This may be used
if the known attacks against convergent encryption are well understood
and the advantages of deterministic identifiers and de-duplication
outweigh. *)

module Encoder : sig
  (** This module provides a streaming encoder that can be used to efficiently encode content piece-wise. The memory used is proportional to the level of the encoded content. The level is related logarithmicly to the size of the content.

@see <http://purl.org/eris#name-streaming-encoding> Streaming Encoding in the ERIS specification *)

  type t
  (** Type of an encoder *)

  val init : convergence_secret:string -> block_size -> t
  (** [init ~convergence_secret block_size] returns a new encoder. *)

  val feed : t -> string -> (ref * block) list
  (** [feed encoder piece] adds [piece] to the encoded content. Returns a list of reference and block pairs that can be emmited.

For every returned [(ref, block)] pair it holds that [ref = Blake2b-256(block)]. As the hash is computed during encoding it is returned so that storage and transport layers do not have to recompute it.

There is no restriction on the size of [piece]. For best performance pieces should be the same size as the block size.
   *)

  val finalize : t -> (ref * block) list * Read_capability.t
  (** [finalize encoder] finishes encoding some content and returns remaing blocks as well as a read capability. *)
end

val encode_string :
  convergence_secret:string ->
  block_size:block_size ->
  string ->
  (ref * block) list * Read_capability.t
(** [encode_string ~convergence_secret ~block_size content] encodes [content] using the provided convergence-secret and block size and returns a list of blocks as well as a read capability. *)

(** {1 Decoding} *)

(** Content can be decoded given the read capability and access to blocks. *)

module Decoder : sig
  (** This module provides a random-access decoder that can be used to efficiently decode pieces of content at given positions.

@see <http://purl.org/eris#name-random-access-decoding> Random-Access Decoding in the ERIS specification.
   *)

  type t
  (** Type of a decoder *)

  val init : block_get:(ref -> block Lwt.t) -> Read_capability.t -> t
  (** [init ~block_get read_capability] returns a new decoder.

Initializing a decoder will not cause any block de-references (the [block_get] function will not be called). *)

  val pos : t -> int
  (** [pos decoder] returns the position of the decoder as offset from the start of the encoded content. *)

  val seek : int -> t -> t Lwt.t
  (** [seek pos decoder] returns a new decoder that is positioned at [pos].*)

  val length : t -> int Lwt.t
  (** [length decoder] returns the length of the encoded content. *)

  val read : int -> t -> (string * t) Lwt.t
  (** [read n decoder] reads at most [n] bytes of encoded content from position of [decoder]. Returns the decoded content as string as well as a new decoder with position set directly after end of content returned. *)
end

val decode_string :
  block_get:(ref -> block Lwt.t) -> Read_capability.t -> string Lwt.t
(** [decode_string ~block_get read_capability] returns the entire decoded content as string. *)
