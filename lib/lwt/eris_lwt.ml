(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
include Eris
module Decoder = Eris.IODecoder.Make (Lwt)

let decode_string ~block_get read_capability =
  let block_size = Read_capability.block_size read_capability in

  (* allocate a buffer ten times the size of block_size - a ungrounded heuristic *)
  let buffer = Buffer.create (block_size * 10) in

  let rec do_read decoder =
    let* s, decoder = Decoder.read block_size decoder in
    Buffer.add_string buffer s;
    (* stop reading if read string is smaller than block size *)
    if String.length s < block_size then return_unit else do_read decoder
  in

  let decoder = Decoder.init ~block_get read_capability in
  let* () = do_read decoder in

  return @@ Buffer.contents buffer
