(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** CBOR Serialization of ERIS Encoded Content *)

(** @see <https://eris.codeberg.page/eer/eer-007> CBOR Serialization of ERIS Encoded Content *)

val to_cbor :
  ?read_capabilities:Eris.Read_capability.t list ->
  (Eris.ref * Eris.block) list ->
  Cborl.item

val of_cbor :
  Cborl.item -> (Eris.ref * Eris.block) list * Eris.Read_capability.t list
