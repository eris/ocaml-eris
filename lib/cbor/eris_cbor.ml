(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let to_cbor ?(read_capabilities = []) blocks =
  Cborl.Array
    ([
       Cborl.Map
         (List.map
            (fun (ref, block) -> (Cborl.ByteString ref, Cborl.ByteString block))
            blocks);
     ]
    @ List.map
        (fun read_capability ->
          Cborl.Tag
            ( Z.(~$276),
              Cborl.ByteString (Eris.Read_capability.to_binary read_capability)
            ))
        read_capabilities)

let of_cbor item =
  match item with
  | Cborl.Array (Cborl.Map blocks :: read_capabilities) ->
      let blocks =
        List.map
          (function
            | Cborl.ByteString r, Cborl.ByteString block -> (r, block)
            | _ -> raise @@ Invalid_argument "invalid CBOR items in blocks")
          blocks
      in
      let read_capabilities =
        List.map
          (function
            | Cborl.Tag (tag, Cborl.ByteString brc) when Z.(equal tag ~$276) ->
                Eris.Read_capability.of_binary brc
            | _ -> raise @@ Invalid_argument "invalid read capability")
          read_capabilities
      in
      (blocks, read_capabilities)
  | _ -> raise @@ Invalid_argument "not a valid encoding"
