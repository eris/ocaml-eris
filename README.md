# ocaml-eris

OCaml implementation of the [Encoding for Robust Immutable Storage (ERIS)](http://purl.org/eris).

This library can be used to encoded and decode content with ERIS. Features include:

- Streaming encoding
- Random-access decoder
- Cross-platform support

## Cryptographic Primitives

We provide multiple implementations of the necessary cryptographic primitives (Blake2b and ChaCha20). End-users can choose which implementation to use by specifying the package in their dependencies. Currently following implementations are provided:

- `eris.crypto-monocypher`: Uses the [Monocypher](https://monocypher.org/) cryptographic library via the [ocaml-monocypher](https://inqlab.net/git/ocaml-monocypher.git/) bindings. This implementation works well on the Unix platform and is selected by default.
- `eris.crypto-zig`: Uses the cryptographic primitives provided in the [Zig](https://ziglang.org/) standard library. This implementation works on the Unix platform and requires the Zig compiler installed.
- `eris.crypto-wasm`: Uses the cryptographic primitives provided by the Zig standard library compiled to WASM. This implementation can be used with [js_of_ocaml](https://ocsigen.org/js_of_ocaml/) and requires the Zig compiler installed. The WASM code is bundled using [Crunch](https://github.com/mirage/ocaml-crunch) so users do not need to worry about provisioning or loading WASM.

A pure OCaml implementation of Blake2b and ChaCha20 would be very nice.

See also the [Digestif](https://github.com/mirage/digestif) project that uses the same linking trick.

## License

[AGPL-3.0-or-later](./LICENSES/AGPL-3.0-or-later.txt)
